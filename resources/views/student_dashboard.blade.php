@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">Welcome Parent</div>
                @if($role_id==3)
                    <div class="col-md-3">
                        <form class="form-horizontal" id="getChild" action="{{url('/student_dashboard/')}}" method="post">
                              {{ csrf_field() }}
                              
                              Select Child: <select name="stundet_user" id="getChild" class="form-control childSelect">
                                @foreach($children as $id=>$name)
                                  <option value="{{ $id }}">{{$name}}</option>
                                @endforeach
                              </select>
                              <input type="submit" value="Submit">
                        </form>
                    </div>
                @endif

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                 <div class="card-header">Name : {{$child_detail['name']}}</div>   
                 <br>
                 <div class="card-header">Class Name : {{$classroom['name']}}</div>   
                 <br>
                 <div class="card-header">Attendance</div> 
                 <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Faculty Name</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($attendance as $_attendance)
                        <tr>
                            <td>{{$_attendance['class_teacher']['name']}}</td>
                            @if($_attendance['status']==1)
                            <td>Present</td>
                            @elseif($_attendance['status']==2)
                            <td>Absent</td>
                            @else
                            <td>Leave</td>
                            @endif
                            <td>{{$_attendance['attendance_date']}}</td>
                            <td>{{$_attendance['attendance_time']}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                </table>
                <br>
                 <div class="card-header">Periods</div>
                  <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Class</th>
                            <th>Faculty Name</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($class_periods as $_class_periods)
                        <tr>
                            <td>{{$_class_periods['class_name']['name']}}</td>
                            <td>{{$_class_periods['class_teacher']['name']}}</td>
                            <td>{{$_class_periods['start_time']}} To {{$_class_periods['end_time']}}</td>    
                        </tr>
                        @endforeach
                        </tbody>
                </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

 @section('javascript')
    <script type="text/javascript">
         $(document).on("change",".childSelect",function(){
            $("#getChild").submit();
          });
    </script>
 @endsection

