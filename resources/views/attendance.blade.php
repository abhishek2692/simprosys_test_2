@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{ URL::previous() }}">Go Back</a>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                 
                 <div class="card-header">Attendance</div> 
                 <table id="example" class="display" border="1" style="width:100%">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($attendance as $_attendance)
                        <tr>
                            <td>{{$_attendance['student_name']['name']}}</td>
                            @if($_attendance['status']==1)
                            <td>Present</td>
                            @elseif($_attendance['status']==2)
                            <td>Absent</td>
                            @else
                            <td>Leave</td>
                            @endif
                            <td>{{$_attendance['attendance_date']}}</td>
                            <td>{{$_attendance['attendance_time']}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                </table>
                
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

 @section('javascript')
    <script type="text/javascript">
         $(document).on("change",".childSelect",function(){
            $("#getChild").submit();
          });
    </script>
 @endsection

