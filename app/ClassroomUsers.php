<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Helper;

class ClassroomUsers extends Model
{
  protected $table = 'class_users';

  public function class_name()
    {
      return $this->hasOne('App\Classroom', 'id', 'class_id');
    }
}