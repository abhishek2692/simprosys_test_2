<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Helper;

class Classroom extends Model
{
  protected $table = 'classrooms';
}