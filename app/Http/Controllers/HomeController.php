<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Session, Helper;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


     public function processlogin()
    {
        // Loggedin user
        $user = Auth::user();
        
        
        Session::put('user_id', $user->id);
        Session::put('user_name', $user->name);
        Session::put('parent_id', $user->parent_id);
        Session::put('role_id', $user->role_id);

        if($user->role_id == 2 || $user->role_id == 3)
        {   
            return redirect('/student_dashboard');
        }else{
            return redirect('/dashboard');
        }

    }

}
