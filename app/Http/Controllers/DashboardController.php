<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Session;
use App\Helpers\Helper;
use App\User;
use App\Classroom;
use App\ClassroomUsers;
use App\ClassroomAttendance;
use App\ClassroomPeriods;
class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
       
       $user_id = Helper::current('user_id');
       


       $UserProfile = User::where('id', $user_id)->first();


       $profile = array();
       $profile['name'] = $UserProfile ? $UserProfile->name : "";
       $profile['email'] = $UserProfile ? $UserProfile->email : '';
       
       $classrooms = [];
       

       if($UserProfile->role_id == 1)
       {
         $role = 'Teacher';
		 $classroom = Classroom::where('class_teacher_id',$user_id)->where('status', 1)->first();
		 $classrooms['name']= $classroom ? $classroom->name : "";
         $profile['role'] = $role;
	   }
		
	   $role_id = Helper::current('role_id');
       
	   return view('dashboard', compact('profile','classrooms'));
    }

    public function student_dashboard(Request $request)
    {
       
       
       $user_id = Helper::current('user_id');
       $student_user_id = ($request->has('stundet_user')) ? $request->input('stundet_user') : 0;

	   $role_id = Helper::current ( 'role_id' );
	   $children[]=array();
	   if($role_id == 3){
	           $user = User::where('parent_id',$user_id)->get();
	           $children=$user->pluck('name','id');
	           
	       	   $current_user = $user;

	           if($student_user_id== 0)
	           {
	          		$child = $user->first();
	          		$user_id=$child['id'];
	           }
	           else{
	           		$user_id=$student_user_id;	
	           }
		}

	   	$child_detail=User::where('id',$user_id)->with('student_classroom.class_name')->first();
	    $classroom['id'] = $child_detail->student_classroom['class_name']['id'];
	    $classroom['name']=$child_detail->student_classroom['class_name']['name'];
	    $attendance = ClassroomAttendance::where('user_id',$child_detail->id)->with('class_teacher','class_name')->get();
	    $class_periods=ClassroomPeriods::where('class_id',$classroom['id'])->with('class_teacher','class_name')->get();
	 	
	 	return view('student_dashboard', compact('attendance','classroom','class_periods','children','role_id','child_detail'));   



    }

    public function student_attendance(Request $request)
    {
       
       
       $user_id = Helper::current('user_id');

       $role_id = Helper::current ('role_id');
	   
	   if($role_id == 1){
	   		$classroom = Classroom::where('class_teacher_id',$user_id)->where('status', 1)->first();
	        $attendance = ClassroomAttendance::where('class_id',$classroom['id'])->with('student_name','class_name')->get();
	    	
	    }

	   	return view('attendance', compact('profile','classroom','attendance'));
	}
	
	public function periods(Request $request)
    {
       
       
       $user_id = Helper::current('user_id');

       $role_id = Helper::current ('role_id');
	   
	   if($role_id == 1){
	   		$classroom = Classroom::where('class_teacher_id',$user_id)->where('status', 1)->first();
	        $class_periods=ClassroomPeriods::where('class_id',$classroom['id'])->with('class_name')->get();
	    	
	    }

	   	return view('periods', compact('profile','classroom','class_periods'));
	}

	//		           


}