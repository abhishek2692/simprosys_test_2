<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Helper;

class ClassroomAttendance extends Model
{
  protected $table = 'attendance';

  public function class_name()
    {
      return $this->hasOne('App\Classroom', 'id', 'class_id');
    }
  public function class_teacher()
    {
        return $this->hasOne('App\User', 'id', 'teacher_id');
    }  
  public function student_name()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }    

}