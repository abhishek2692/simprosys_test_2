<?php

namespace App\Helpers;
use Session;
use Auth;
use Request;

class Helper{

	 # get current loggedin user's data
    public static function current($key = "") {
        $return = [
            'user_id' => Session::get('user_id'),
            'parent_id' => Session::get('parent_id'),
            'role_id' => Session::get('role_id'),
        ];

        if (!empty($key) && isset($return[$key])) {
            return $return[$key];
        }

        return $return;
    }

}