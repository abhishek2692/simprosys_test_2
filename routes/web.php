<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    Route::get('processlogin', 'HomeController@processlogin');
    Route::get('/dashboard', 'DashboardController@index');
    Route::match(['get', 'post'],'student_dashboard', 'DashboardController@student_dashboard');
    Route::get('/attendance', 'DashboardController@student_attendance');
    Route::get('/periods', 'DashboardController@periods');
    
    
});
